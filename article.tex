%
\documentclass[a4paper]{esannV2}
\usepackage{graphicx}%[dvips]
\usepackage{caption}
\usepackage{subcaption}
\usepackage[utf8]{inputenc}
\usepackage{amssymb,amsmath,array}
\usepackage{multirow}
\usepackage{natbib}

\DeclareMathOperator*{\argmin}{arg\,min}

\renewcommand{\baselinestretch}{0.93}
\newcommand{\gap}{\vspace{-1em}}
\newcommand{\smallgap}{\vspace{-0.5em}}

%***********************************************************************
% !!!! IMPORTANT NOTICE ON TEXT MARGINS !!!!!
%***********************************************************************
%
% Please avoid using DVI2PDF or PS2PDF converters: some undesired
% shifting/scaling may occur when using these programs
% It is strongly recommended to use the DVIPS converters, and to submit
% PS file. You may submit a PDF file if and only if you use ADOBE ACROBAT
% to convert your PS file to PDF.
%
% Check that you have set the paper size to A4 (and NOT to letter) in your
% dvi2ps converter, in Adobe Acrobat if you use it, and in any printer driver
% that you could use.  You also have to disable the 'scale to fit paper' option
% of your printer driver.
%
% In any case, please check carefully that the final size of the top and
% bottom margins is 5.2 cm and of the left and right margins is 4.4 cm.
% It is your responsibility to verify this important requirement.  If these margin requirements and not fulfilled at the end of your file generation process, please use the following commands to correct them.  Otherwise, please do not modify these commands.
%
\voffset 0 cm \hoffset 0 cm \addtolength{\textwidth}{0cm}
\addtolength{\textheight}{0cm}\addtolength{\leftmargin}{0cm}

%***********************************************************************
% !!!! USE OF THE esannV2 LaTeX STYLE FILE !!!!!
%***********************************************************************
%
% Some commands are inserted in the following .tex example file.  Therefore to
% set up your ESANN submission, please use this file and modify it to insert
% your text, rather than staring from a blank .tex file.  In this way, you will
% have the commands inserted in the right place.

\begin{document}
%style file for ESANN manuscripts
\title{Incremental learning algorithms and applications}

%***********************************************************************
% AUTHORS INFORMATION AREA
%***********************************************************************
\author{Alexander Gepperth$^1$, Barbara Hammer$^2$
%
% Optional short acknowledgment: remove next line if non-needed
\thanks{This research/work was supported by the Cluster of Excellence Cognitive Interaction Technology 'CITEC' (EXC 277) at Bielefeld University, which is funded by the German Research Foundation (DFG). Alexander Gepperth is also with INRIA FLOWERS.}
%
% DO NOT MODIFY THE FOLLOWING '\vspace' ARGUMENT
\vspace{.2cm}\\
%
% Addresses and institutions (remove "1- " in case of a single institution)
1- UIIS, ENSTA ParisTech\\
INRIA, Université Paris-Saclay\\
828 Bvd des Mar\'echaux, 91762 Palaiseau Cedex, France\\
%
2- Bielefeld University, CITEC centre of excellence\\
Universit\"atsstrasse 21-23\\
D-33594 Bielefeld, Germany\\
}
%***********************************************************************
% END OF AUTHORS INFORMATION AREA
%**************************************************************s*********

\maketitle

%\gap
\begin{abstract}
Incremental learning refers to learning from streaming data, which arrive over time,
with limited memory resources and, ideally, without sacrificing model accuracy.
This setting fits different application scenarios where lifelong learning is relevant, e.g.\ due to
changing environments, and it offers an elegant scheme for 
big data processing by means of its sequential treatment. In this contribution,
we formalise the concept of incremental learning, we discuss particular
challenges which arise in this setting, and we give an overview about popular
approaches, its theoretical foundations, and applications which emerged in the last years.
\end{abstract}

%\gap
%
% ------------------------------------------
\section{What is incremental learning?}
% ------------------------------------------
%
Machine learning methods offer particularly powerful technologies to infer structural information from
given digital data; still, the majority of current applications restrict to the classical batch setting: data
are given prior to training, hence meta-parameter optimisation and model selection can be based on the full
data set, and training can rely on the assumption that the data and its underlying structure are static. 
Lifelong learning, in contrast, refers to the situation of continuous model
adaptation based on a constantly arriving data stream \cite{DBLP:conf/aaaiss/2013-5,DBLP:conf/agi/Silver11}.
This setting is present whenever systems act autonomously such as in autonomous robotics or driving 
\cite{Thrun:2010:TRC:1721654.1721679,Amirat20161,Menegatti2015297,KI15HamTou}.
Further, online learning becomes necessary in any interactive scenario where training examples are provided 
based on  human feedback over time \cite{Rico-Juan2014316}.
Finally, many digital data sets, albeit static, can become so big that they are de facto dealt with 
as a data stream, i.e.\ one incremental pass over the full data set \cite{JMLR:v16:morales15a}.
\emph{Incremental learning} investigates how to learn in such a streaming setting.
It comes in various forms in the literature, and the use of the term is not always consistent. 
Therefore, first, we give a meaning to the terms
\emph{online learning}, \emph{incremental learning},
and \emph{concept drift}, 
%
% added
giving particular attention to the supervised learning paradigm.


%
% ------------------------------------------
\subsection{Online learning methods}
% ------------------------------------------
%
In supervised learning, data $ {\cal D} = 
((\vec x_1,y_1),(\vec x_2,y_2), (\vec x_3,y_3),\ldots,(\vec x_m,y_m))$ are available
with input signals $\vec x_i$ and outputs $y_i$.
The task is to infer a model $M \approx p(y|\vec x)$ from such data.
Machine learning algorithms are often trained  in a \emph{batch} mode, i.e.,
they use all examples $(\vec x_i,y_i)$ at the same time, irrespective of their (temporal) order, 
to perform, e.g., a model optimisation step. 

%\gap
%
% ------------------------------------------
\paragraph*{{\bf Challenge 1: Online model parameter adaptation.}}
% ------------------------------------------
%
In many application examples, data ${\cal D}$ are not available priorly, but examples arrive over time,
and the task is to infer a reliable model $M_t$ after \emph{every} time step based on the example
$(\vec x_t,y_t)$ and the previous model $M_{t-1}$ only.
This is realised by 
\emph{online learning} approaches, which use training samples one by one, without knowing their number in advance, 
to optimise their internal cost function. There is a continuum of possibilities here, ranging from fully online 
approaches that adapt their internal model immediately upon processing of a single sample, over so-called 
{\it mini-batch} techniques that accumulate a small number of samples, to batch learning 
approaches, which store all samples internally.

Online learning is easily achieved by stochastic optimisation techniques such as
online back-propagation, but 
there are also extensions of the support vector machine (SVM) \cite{wen2007}. 
Prototype-based models such as vector quantisation, radial basis function networks (RBF), supervised learning vector quantisation, and self-organising maps  (SOM) all naturally realise online 
learning schemes since they  rely on a (approximate) stochastic gradient technique
\cite{runkler2012,rbf,lvq,Kohonen1982}.
Second order numeric optimisation methods and advanced optimisation schemes
can be extended as well,
such as variational Bayes, convex optimization, second order perceptron learning based on higher order statistics in primal or dual space,
and online realisations of 
the quasi-Newton Broyden-Fletcher-Goldfarb-Shanno technique \cite{JMLR:v16:mokhtari15a,JMLR:v16:moroshko15a,DBLP:conf/nips/GentileVB07,Penalver2012534,Hall2015647}. 
Stochastic optimization schemes can be developed also for non-decomposable cost function,
\cite{Kar2014694}. Further, lazy learners 
such as k-nearest neighbour (k-NN) methods lend itself to
online scenarios by their design \cite{runkler2012}.
Interestingly, online learning has already very early been accompanied by their exact
mathematical investigations \cite{Watk93a}.


%
% ------------------------------------------
\subsection{Incremental learning methods}
% ------------------------------------------
%
\emph{Incremental learning} refers to online learning strategies which work with
\emph{limited memory resources}. This rules out approaches
which essentially work in batch mode for the inference of $M_t$ by storing all examples up to time step $t$
in memory; rather, incremental learning
has to rely on a compact representation of the already observed signals,
such as an efficient statistics of the data, an alternative compact memory model,
or an implicit data representation in terms of the model parameters itself.
At the same time, it has to provide accurate results for all relevant settings, despite its limited
memory resources.

\gap
%
% ------------------------------------------
\paragraph*{{\bf Challenge 2: Concept drift.}}
% ------------------------------------------
%
Incremental learning shares quite a number of challenges with online learning,
with memory limitations adding quite a few extras. One prominent problem consists in the fact
that, 
when the temporal structure of data samples is taken into account, one can observe changes in data statistics that occur over time, i.e.\ samples $(\vec x_i,y_i)$ are not i.i.d.
Changes in the data distribution over time are commonly referred to as \emph{concept drift}~\cite{drift2014,tsymbal2004problem,Ditzler201512,Polikar20149}.
Different types of concept drift can be distinguished:
changes in the input distribution
$p(\vec x)$ only, referred to as \emph{virtual concept drift} or \emph{covariate shift},
or changes in the underlying  functionality itself $p(y|\vec x)$, referred to as \emph{real concept drift}.
Further, concept drift can be
gradual or abrupt. In the latter case one often uses the term \emph{concept shift}. 
The term \emph{local concept drift} characterises changes of the data statistics  only in a specific region of data space
 \cite{tsymbal2004problem}. A prominent example is the addition of a new, visually dissimilar object class to a classification problem.
Real concept drift is problematic since it leads to conflicts in the classification, for example when a new but visually similar class appears in the data: this will in any event have an impact on classification performance until the model can be re-adapted accordingly.
\gap
%
% ------------------------------------------
\paragraph*{{\bf Challenge 3: The stability-plasticity dilemma.}}
% ------------------------------------------
%
In particular for noisy environments or concept drift, a second challenge consists in the question
when and how to adapt the current model. 
A quick update enables a rapid adaptation according to new information,
but old information is forgotten equally quickly. On the other hand, adaption can be performed slowly, in which case old information is retained longer but the reactivity of the system is decreased. The dilemma behind this trade-off is usually denoted the \emph{stability-plasticity dilemma}, which is a well-known constraint for artificial  as
well as biological learning systems \cite{stability}.
Incremental learning techniques, which adapt learned models to concept drift only in those regions of the data space where concept drift actually occurs, offer a partial remedy to this problem. 
Many online learning methods alone, albeit dealing with  limited resources,
are not able to solve this dilemma 
since they exhibit a so-called \emph{catastrophic forgetting} behaviour \cite{cata0,cata0b,cata1,cata2,cata3}
even when the new data statistics do not invalidate the old ones. 

One approach to deal with the stability-plasticity dilemma consists in the enhancement of the 
learning rules by explicit meta-strategies, when and how to learn.
This is at the core of popular incremental models such as ART networks
\cite{Impedovo20141002,Grossberg20131}, or meta-strategies to deal with concept drift
such as the just-in-time classifier JIT \cite{Alippi2009114},
or hybrid online/offline methods \cite{Nguyen20091401,Fischer2015}.
One major ingredient of such strategies consist in a confidence estimation
of the actual model prediction, such as statistical tests, efficient surrogates,
or some notion of self-evaluation  \cite{Bae20141218,Fischer2015,Jauffret2013}.
Such techniques can be enhanced to complex incremental schemes
for interactive learning or learning scaffolding  \cite{Pratama201689,Kompella20142705}.

\gap
%
% -----------------------------------
\paragraph*{{\bf Challenge 4: Adaptive model complexity and meta-parameters.}}
% -----------------------------------
% 
For incremental learning,
model complexity must be variable, since it is impossible to estimate the model complexity in advance
if the data are unknown.
Depending on the occurrence of concept drift events, an increased model complexity might become necessary.
On the other hand, the overall model complexity is usually bounded from above 
by the limitation of the available resources. This requires the intelligent reallocation of resources whenever
this  limit is reached. 
Quite a number of approaches propose intelligent 
adaptation methods for the model complexity such as 
incremental architectures \cite{Wu20151659}, self-adjustment of the number of basic units in
extreme learning machines \cite{Ding2015215,Zhang2012365} or
prototype-based models
\cite{Impedovo20141002,AMAI15Schetal,Losing2015},
incremental base function selection for a sufficiently powerful data
representation \cite{Chen2014356}, or self-adjusting cluster numbers in unsupervised
learning \cite{Kalogeratos20122393}. Such strategies can be put into the more
general context of self-evolving systems, see e.g.\ 
\cite{Lemos2013117} for an overview.
An incremental model complexity is not only mandatory whenever
concept drift is observed, hence a possibly changing model complexity is present, but
it can also dramatically speed-up learning in batch scenarios, since
it makes often tedious model selection superfluous.

In batch learning, not only the model complexity, but also essential meta-parameters such as learning 
rate, degree of error, regularisation constants, etc.\ are determined prior to
training. Often, time consuming cross-validation is used in batch learning, whereby
first promising result
how to automate the process have been presented
\cite{ThoHutHooLey13-AutoWEKA}. However, these are not suited for incremental learning scenarios:
Concept drift turns critical meta-parameters  such as the learning rate into model parameters,
since their choice has to be adapted according to the (changing) data characteristics.
Due to this fact, incremental techniques often rely on models with robust meta-parameters
(such as ensembles), or they use meta-heuristics how to adapt
these quantities during training.

\gap
%
% -----------------------------------
\paragraph*{{\bf Challenge 5: Efficient memory models.}}
% -----------------------------------
%
Due to their limited resources, incremental learning models have to store the
information provided by the observed data in compact form. This can be done via
suitable system invariants (such as the classification error for explicit drift detection models
\cite{Ditzler201512}), via the model parameters in implicit form (such as prototypes for distance-
based models \cite{NC10HasHam}), or via an explicit memory model
\cite{Liu20155054,Losing2015}.
Some machine learning models offer a seamless transfer 
of model parameters and memory models, such as prototype-\ or exemplar--based models, which
store the information in the form of typical examples \cite{NC10HasHam}. 
Explicit memory models can rely on a finite window of characteristic training examples, or 
represent the memory in the form of a parametric model.
For both settings, a careful design
of the memory adaptation  is crucial since it directly mirrors the
stability-plasticity dilemma
\cite{Liu20155054,Losing2015}.

\gap
%
% -----------------------------------
\paragraph*{{\bf Challenge 6: Model benchmarking.}}
% -----------------------------------
%
There exist two fundamentally different possibilities to assess
the performance of incremental learning algorithms:
%on the one hand, 
%one could ask how an incremental algorithm performs in relation to a non-incremental one when trained on the same task. On the other hand, one may wish to compare the ability to learn incrementally between different incremental learning algorithms. In both cases, different benchmarks are necessary, since, in the
%context of concept drift, these settings are different. 

%
% ppppppppppppppppppppppppppppppppppp
\noindent
\emph{(1) Incremental -vs- non-incremental:}\quad
% ppppppppppppppppppppppppppppppppppp
%
In particular in the absence of concept drift,
the aim of learning consists in the inference of the stationary distribution $p(y|\vec x)$ for 
typical data characterised by $p(\vec x)$. This setting occurs e.g.\ whenever
incremental algorithms are used for big data sets, where they compete with
often parallelized batch algorithms. In such settings, the method of choice
evaluate the classification accuracy of the final model $M_t$ 
on a test set, or within a cross-validation.
While incremental learning should attain results in the same range as batch variants,
one must take into account that they deal with restricted knowledge
due to their streaming data access.  
It has been shown, as an example, that incremental clustering algorithms cannot reach the same accuracy as
batch versions if restricted in terms of their resources
\cite{DBLP:conf/nips/AckermanD14}.

%
% ppppppppppppppppppppppppppppppppppp
\noindent
\emph{(2) Incremental -vs- incremental:}\quad
% ppppppppppppppppppppppppppppppppppp
%
When facing concept drift, different cost functions can be of interest.
Virtual concept drift aims for the inference of a stationary 
model $p(y|\vec x)$
with drifting probability $p(\vec x)$ of the inputs. In such settings, 
the robustness of the model when evaluated on test data which follow a possibly skewed distribution
is of interest. Such settings can easily be generated e.g.\ by enforcing imbalanced
label distributions for test and training data \cite{Hoens2012168}.
Whenever real confidence drift is present, the online behaviour of the classification error
$\|M_t(\vec x_{t+1})-y_{t+1}\|$ for the next data point is usually the method
of choice; thereby,  a simple average of these errors can be accompanied by a detailed inspection
of the overall shape of the online error, since it provides insight into the rates of convergence 
e.g.\ for abrupt concept drift.

%
% ppppppppppppppppppppppppppppppppppp
\noindent
\emph{(3) Formal guarantees on the generalisation behaviour:}\quad
% ppppppppppppppppppppppppppppppppppp
%
Since many classical algorithms such  as the simple perceptron or large
margin methods have been proposed as online algorithms, there exists an extensive
body of work investigating their learning behaviour,
convergence speed, and generalisation ability, classically 
relying on the assumption of data being i.i.d.\ \cite{Watk93a}.
Some results weaken the i.i.d.\
assumption e.g.\ requiring only interchangeability
\cite{DBLP:journals/jmlr/ShaferV08}.
Recently, popular settings such as learning 
a (generalised) linear regression could be accompanied by
convergence guarantees for arbitrary distributions $p(\vec x)$
by taking a game theoretic point of view: in such settings,
classifier $M_t$ and training example $\vec x_{t+1}$ 
can be taken in an adversial manner, still allowing 
fast convergence rates in relevant situations
\cite{JMLR:v16:vanerven15a,JMLR:v16:rakhlin15a,Kuhn201569,WICS:WICS1275}
The approach
\cite{JMLR:v16:moroshko15a} even provides first theoretical
results for real context drift, i.e.\ not only the
input distribution, but also the conditional distribution
$p(y|\vec x)$ can follow mild changes.

% 
% *****************************************************************************
\section{Incremental learning models}\label{sec:incr:relwork}
% *****************************************************************************
Incremental learning comes in various forms in the literature, and the use of the term is not always consistent;
for some settings, as an example, a memory limitation cannot be guaranteed, 
or models are designed for stationary distributions only.
We will give an overview over popular models in this context.
Thereby, we will mostly focus on supervised methods due to its popularity.
Online or incremental learning techniques have also been developed for alternative
tasks such as clustering  \cite{Mehrkanoon201588,Langone2014246},
dimensionality reduction \cite{Cui20151198,Bassiou20141953,Choi2014280,AnakJoseph20143135,Ozawa20092394,Leng20143100},
feature selection and data representation\cite{Zuniga20133,Guan20121087,Ferreira201460,Zeng20143726,Degeest2015,He2015},
reinforcement learning
\cite{Barreto20121484,Guan2014471},
mining and inference \cite{Gomes201495,Pratama201455}.

\gap
%
% 
% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
\paragraph*{{\bf Explicit treatment of concept drift.}}
% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%
Dealing with concept drift at execution time constitutes a challenging task \cite{drift2014,tsymbal2004problem,Ditzler201512,Polikar20149}. 
There exist different techniques to address concept drift, depending on its type.
Mere concept shift is often addressed by so-called passive methods, i.e.\
learning technologies which smoothly adapt model parameters such that the current distribution is
reliably represented by the model. Rapid concept changes, however, often require active methods,
which detect concept drift and react accordingly.

Virtual concept drift, which concerns the input distribution only, can easily occur e.g.\
due to highly imbalanced classes over time. One popular state-of-the-art technology accounts for this fact by
so-called importance weighting, i.e.\ strategies which explicitly or implicitly re-weight the observed samples
such that a greater robustness is achieved
\cite{DBLP:journals/ieicet/KawakuboPS16,DBLP:journals/prl/BalziYS15,Hoens2012168}.
Alternatively, concept shift can have its reason in novelty within the data or even new classes. 
Such settings can naturally
be incorporated into local models provided they offer an adaptive model complexity \cite{AMAI15Schetal,Fischer2015,Grossberg20131,Reiner2015349,Lu20142142}.

Real concept drift can be detected by its effect on characteristic features of the model
such as the classification accuracy. Such quantitative features can be accompanied by 
statistical tests which can judge the significance of their chance, hence concept drift. 
Tests can rely on well-known statistics such as the Hoeffding bound
\cite{Fras-Blanco2015810}, or alternatively on suitable distances such as
the Hellinger distance, which can measure the characteristics of value distributions
of such characteristic features.
When integrated into robust classifiers such as ensemble techniques,
models which can simultaneously deal with different types 
of drift result
\cite{Brzezinski201481}.

\gap
%
% ----------------------------------
\paragraph*{{\bf Support vector machines and generalised linear models.}}
% ----------------------------------
%
Several incremental SVM models exist \cite{wen2007}. Some rely on heuristics, like retraining a model with all support vectors plus a new "incremental" batch of data \cite{989572,syed1999a}, but without theoretical guarantees. Other incorporate modification of the SVM cost function  to facilitate incrementality \cite{989589}
and also possibly control complexity \cite{Gu2015140,Gu20151403}. 
Still, their resources are not strictly limited.
As an alternative,
{\it adiabatically} SVM training has been proposed, i.e., presenting one example at a time while maintaining the relevant optimality conditions on all previously seen examples. However this requires all previously seen samples need to be stored, although the approach can considerably 
simplify SVM training.
Ensemble learning algorithms based on SVM \cite{polikar2001learn++,wen2007} achieve incremental learning by training new classifiers for new batches of data, and combining all existing classifiers only for decision making. 
Another hybrid scheme combines a SVM classifier with a prototype-based data representation, whereby the latter can
be designed as an online model based on which training examples
for SVM can be generated \cite{Xing2015}.
Alternatively, SVMs can directly be trained in primal space,
where online learning is immediate
\cite{Chapelle:2007:TSV:1246422.1246423}.
Online versions have also been proposed for more complex generalised
linear models such as Gaussian Process regression
\cite{Gijsberts201359,Meier2014972}, whereby none of these models can yet easily deal with concept drift.

\gap
%
% ----------------------------------
\paragraph*{{\bf Connectionist models.}}
% ----------------------------------
%
As the problem of catastrophic forgetting was first remarked for multilayer perceptrons (MLP) \cite{cata0,cata0b}, it is hardly surprising that there exists 
significant work how to avoid it in connectionist systems. 
Initial consensus traced catastrophic forgetting back to
their {\it distributed} information representation \cite{french}. 
Indeed, {\it localist} connectionist models such as radial basis function (RBF) networks can work reliably in
incremental settings 
\cite{Reiner2015349,Lu20142142}, whereby care has to be taken to guarantee their generalisation
performance \cite{redgen1}.
Both capabilities are combined in {\it semi-distributed representations}. A number of algorithmic modifications of the MLP model has been proposed, such as
sparsification \cite{cata1}, orthogonalization of internal node weights \cite{ortho1,ortho2}, reduction of representational overlap while training \cite{bp1}, or specific regularisation \cite{goodfellow2013empirical}. These are successful in mitigating but not eliminating catastrophic forgetting \cite{redgen1}. 
%This is not surprising as representational resources are less efficiently exploited if they are constrained to be sparse or orthogonal. In fact this was a first step towards semi-distributed or localist prototype-based (see Sec.~\ref{sec:proto}) representations although a vastly increased number of units is required for these approaches to generalize well.
%A rather recent proposition is made in \cite{goodfellow2013empirical} where a specific regularization scheme is supposed to reduce but in no way eliminate catastrophic forgetting effects.
Recently, there has been an increased interest in extreme learning machines (ELM), which combine a random mapping
with a trained linear readout.
Due to their simple training,
incremental variants can easily be formulated, whereby their reservoir naturally represents 
rich potential concepts
\cite{Zhou2014,Ding2015215,vanSchaik2015233,Guo201450}.

Furthermore, there exist attempts to modify the system design of MLPs \cite{arch1,arch2} which are more in the line of generative learning; they incorporate  novelty detection and use different representational resources for new samples. Elaborate
connectionist models feature different memory subsystems for long-\ and short-term learning \cite{lstm1,lstm2}, as well as explicit replay and re-learning of previous samples to alleviate forgetting \cite{rehearse}. 
These approaches reduce the problem of catastrophic forgetting at the price of vastly more complex model.
Contrarily to other modern approaches, inspiration is taken primarily from biology and thus its solid mathematical understanding
is yet lacking. 

\gap
%
%
% ----------------------------------
\paragraph*{{\bf Explicit partitioning approaches.}}\label{sec:part}
% ----------------------------------
%
Many modern incremental learners rely on a local partitioning of the input space, and  a separate classification/regression model for each partition \cite{vija2000,peters2008,sigaud2011,butz2005,cederborg2010}. The manner of performing this partitioning is very diverse, ranging from kd-trees \cite{cederborg2010} to genetic algorithms \cite{butz2005} and adaptive Gaussian receptive fields \cite{vija2000}. Equally, the choice of local models varies between linear models \cite{vija2000}, Gaussian mixture regression \cite{cederborg2010} or Gaussian Processes \cite{peters2008}.
For high-dimensional problems such as occur in perception,
the partitioning of the input space constitutes the bottleneck as concerns memory consumption.
Covariance matrices as used in \cite{vija2000}, for example, are quadratic in the number of input dimensions, hence
prohibitive for high dimensional data.

Decision trees partially alleviate this problem insofar as they cut along one dimension only, disregarding
feature correlations. Quite a number of incremental tree builders have been proposed for classification
or regression 
\cite{Fan20151673,Salperwyck2013,Gholipour2013815},
with a particular focus on when to split, how to avoid overly large trees
while ensuring incremental growth, and how to reliably deal with imbalanced classes
\cite{Hapfelmeier20142072,DeRosa2015,Lyon20141969}. 
Interestingly, there do exist tree classifiers which result is entirely
invariant to the ordering of the training
data, but at the price of  unlimited resources \cite{Lakshminarayanan20143140}.

\gap
%
% ----------------------------------
\paragraph*{{\bf Ensemble methods.}}
% ----------------------------------
%
Ensemble methods combine a collection of different models by a suitable weighting strategy.
As such, they are ideally suited to implicitly represent even partially contradictory concepts in parallel
and mediate the current output according to the observed data statistics at hand.
Ensemble methods have proved particularly useful when dealing with concept drift,
with a few popular models ranging from incremental random forests
\cite{Ma20142407}, ensembles of bipartite graph classifiers \cite{Bertini20131802},
up to advanced weighting schemes suitable
for different types of concept drift and recurring concepts
\cite{Yin201514,Mejri20131115,Li201568,Yin201514,Ditzler20132283,Elwell20111517}.

\gap
% ----------------------------------
\paragraph*{{\bf Prototype-based methods.}}\label{sec:proto}
% ----------------------------------
%
Prototype-based machine learning has its counterpart in cognitive psychology 
\cite{prototheory1} which hypothesises that semantic categories in the human mind are represented by specific examples for these categories. In machine learning approaches,
a class is represented by a number of representatives,
and class membership is defined based on the distance of the data from
these prototypes. For high dimensional data,
adaptive low-rank metric learning schemes can dramatically
improve classification accuracy and efficiency \cite{DBLP:journals/nn/BunteSHSVB12,DBLP:journals/neco/SchneiderBH09a}.
Prototype-based methods are a natural continuation of the work on localist or semi-distributed representations in early connectionist models, and thus share many properties. They have the advantage of 
an easily adaptive model complexity.  
One  disadvantage is that the number of prototypes can become large whenever complex
class boundaries are present. 

Prototype-based models are closely connected to 
the non-parametric k-NN classifier (all training points act as prototypes)
and the RBF model \cite{runkler2012}. A popular supervised method is given
by learning vector quantisation (LVQ) and recent variants which can be substantiated by a cost function 
\cite{lvq}. A number of incremental variants and methods capable of dealing with concept drift
have been proposed, such as 
dynamic prototype inversion / deletion schemes \cite{Losing2015,AMAI15Schetal}, 
or techniques with fixed model complexity, but intelligent source redistribution strategies
\cite{gepperth2015bio}.
Similar unsupervised incremental models exist   \cite{NC10HasHam,Cai2014258,
Zhang20141096}.
\gap
%
% ----------------------------------
\paragraph*{{\bf Insights into biological incremental learning.}}\label{sec:incr:bio}
% ----------------------------------
%
As biological incremental learning has reached a high degree of perfection, 
biological paradigms can provide inspiration how to set up artificial incremental systems.
There is evidence that
sensory representations in the neocortex are prototype-based, whereby neurons are topologically arranged by similarity \cite{tanaka1996inferotemporal,leopold2006norm,ross2014not,erickson2000clustering}. 
%This implies a generative approach to representing stimuli 
Learning acts on these representations in a task-specific way insofar as the density of neurons 
is correlated to sensory regions which require finer discrimination \cite{polley2006perceptual}, i.e., where more errors occur. Here, learning is conceivably enhanced through acetylcholine release in case of task failures \cite{Weinberger2003268,hasselmo2006role}. Learning  respects the topological layout by changing only a small subset of neural selectivities \cite{rolls1989effect} at each learning event, corresponding to regions around
the best matching unit \cite{erickson2000clustering}. 

Beyond the single-neuron level, there is a large body of literature investigating the roles of the hippocampal and neocortical areas of the brain in learning at the architectural level. Generally speaking, the hippocampus employs a rapid learning rate with separated representations whereas the neocortex learns slowly, building overlapping representations of the learned task \cite{oreilly04}. A well-established model of the interplay between the hippocampus and the neocortex suggests that recent memories are first stored in the hippocampal system and played back to the neocortex over time \cite{McClelland95}. This accommodates the execution of new tasks that have not been recently performed as well as the transfer of new task representations from the hippocampus (short-term memory) to the neocortical areas (long-term memory) through slow synaptic changes, i.e.\ it provides an architecture which is capable of facing the stability-plasticity dilemma.

%
%
% ****************************************************************************
\section{Applications}
% ****************************************************************************
We would like to conclude this overview by a glimpse on typical application scenarios where
incremental learning plays a major role.

\gap
% ----------------------------------
\paragraph*{{\bf Data analytics and big data processing.}}
% ----------------------------------
There is an increasing interest in single-pass limited-memory models which enable
a treatment of big data within a streaming setting
\cite{esann14Hametal}. Their aim is to reach the
capability of offline techniques, and conditions are less strict as concerns e.g.\ the presence of concept drift.
Recent approaches extend, for example, extreme learning machines in this way \cite{Xin2015464}.
Domains, where this approach is taken, include
image processing
\cite{Doan2013,Liu2016355},
data visualisation \cite{Malik2016127}, and processing of
networked data \cite{Dhanjal2014440}.

\gap
% ----------------------------------
\paragraph*{{\bf Robotics.}}
% ----------------------------------
Autonomous robotics and human-machine-interaction are inherently incremental, since they are 
open-ended, and data arrive as a stream of signals with possibly strong
drift. Incremental learning paradigms have been designed in the realm of
autonomous control \cite{Wang20151247}, 
service robotics
\cite{Amirat20161}, computer vision
\cite{Zhang201693}, self-localisation \cite{Khan20155441}, or interactive
kinesthetic teaching \cite{Saveriano20153570,GhalamzanE.20155616}.
Further, the domain of autonomous driving is gaining enormous speed
\cite{Thrun:2010:TRC:1721654.1721679,Mozaffari2015845,Allamaraju20141161},
with enacted autonomous vehicle legislation in already eight states in the US (Dec.\ 2015).
Another emerging area,  caused by ubiquitous sensors within smart phones,
addresses activity recognition and modeling
\cite{Hasan201624,Abdallah2015304,Kviatkovsky201415,Hu20131051,Loy20121799,Hasan2014796}.

\gap
% ----------------------------------
\paragraph*{{\bf Image processing.}}
% ----------------------------------
Image and video data are often gathered in a streaming fashion, lending itself
to incremental learning.
Typical problems in this context range from
object recognition
\cite{Dou2015382,Bai2015189,Losing2015},
image segmentation \cite{Dou2015382,He20121568},
and image representation
\cite{Diaz-Chito20151761,Wu2015244},
up to video surveillance, person identification, and visual tracking
\cite{Tao20131287,Dewan2016129,Xi-Zhao20133,Ma201583,Dou2015210,Rico-Juan2014316,Zhang2014237,Lu2014132}.

\gap
% ----------------------------------
\paragraph*{{\bf Automated annotation.}}
% ----------------------------------
One important process consists
in the automated annotation or tagging of digital data. This
requires incremental learning approaches as soon as
data arrive over time; example
systems are presented in 
the approaches \cite{Huang20151573,Bianco201588,Carneiro201511}
for video and speech tagging.

\gap
% ----------------------------------
\paragraph*{{\bf Outlier detection.}}
% ----------------------------------
Automated surveillance of technical systems equipped with sensors
sensors constitutes an important task in different domains, starting from process monitoring \cite{Hartert2014118}
fault diagnosis  in technical systems \cite{Yin2014224,Huang2015,Yang2012},
up to  cyber-security \cite{Pang2015}. Typically, a strong drift is present in such settings, hence there is a demand for
advanced incremental learning techniques.

% ****************************************************************************
% BIBLIOGRAPHY AREA
% ****************************************************************************

\renewcommand{\baselinestretch}{0.85}
\setlength{\bibsep}{.2mm}%
\begin{tiny}
\bibliographystyle{abbrv}
\bibliography{incr.bib,babs_incr.bib}
\end{tiny}

% ****************************************************************************
% END OF BIBLIOGRAPHY AREA
% ****************************************************************************

\end{document}
